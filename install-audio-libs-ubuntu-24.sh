#!/bin/bash

# This script is for installing audio-libs and programms for audio editing
# It's for ubuntu 22.4
# Author: Joerg Sorge
# Distributed under the terms of GNU GPL version 2 or later
# Copyright (C) Joerg Sorge
# 2025-01-18

echo ""
echo "Install several audio-libs, pipewire utils and lv2 PlugIns ...

---
This script will

- Take the current user to the group audio
- Install:

	- curl
	- p7zip-full
	- pipewire-jack
	- qpwgraph
	- audacious
	- ffmpeg
	- mp3gain
	- lame
	- Invada PlugIns
	- LSP PlugIns
	- MasterMe PlugIn
"
echo "You may this script running at least on ubuntu 24.04!!!"
echo ""

read -p "Are you sure to proceed? (y/n) " -n 1
echo ""
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
	echo ""
	echo "Installation aborted"
	exit
fi

echo ""
echo "Take user to the audio group"
sudo usermod -aG audio ${USER}

echo ""
echo "Load and install packages..."
echo "You may take a cup of coffee..."
echo ""

sudo apt install \
curl p7zip-full \
pipewire-jack qpwgraph \
audacious ffmpeg \
mp3gain lame

echo ""
echo "Invada PlugIns..."
echo ""
sudo apt install invada-studio-plugins-lv2

echo ""
echo "Donwload and install LSP PlugIns..."
echo ""

current_release=`basename $(curl -Ls -o /dev/null -w %{url_effective} https://github.com/lsp-plugins/lsp-plugins/releases/latest)`

echo ""
echo "Download"
wget https://github.com/lsp-plugins/lsp-plugins/releases/download/$current_release/lsp-plugins-$current_release-Linux-x86_64.7z
7z x lsp-plugins-$current_release-Linux-x86_64.7z

echo ""
echo "Check for ~/.lv2"
if [ ! -d ~/.lv2 ]; then
	echo "mkdir ~/.lv2"
  mkdir ~/.lv2
fi

echo ""
echo "Copy to ~/.lv2"
cp -r lsp-plugins-$current_release-Linux-x86_64/LV2/lsp-plugins.lv2/ ~/.lv2/

echo ""
echo "MasterMe PlugIn..."
echo ""

current_release=`basename $(curl -Ls -o /dev/null -w %{url_effective} https://github.com/trummerschlunk/master_me/releases/latest)`

echo ""
echo "Download"
wget https://github.com/trummerschlunk/master_me/releases/download/$current_release/master_me-$current_release-Linux-x86_64.tar.xz
tar -xf master_me-$current_release-Linux-x86_64.tar.xz

echo ""
echo "Copy to ~/.lv2"
cp -r master_me-$current_release/master_me.lv2/ ~/.lv2/
cp -r master_me-$current_release/master_me-easy-presets.lv2/ ~/.lv2/

echo ""
echo "Finito"
echo "Keep in mind, "
echo "that you have to log out and on "
echo "to have finished adding the user to the audio group"
read -p "Press [Enter] key to finish..."
exit
