#!/bin/bash

# This script is for installing audio-libs and programms for audio editing
# It's for ubuntu 20.4
# Author: Joerg Sorge
# Distributed under the terms of GNU GPL version 2 or later
# Copyright (C) Joerg Sorge
# 2022-01-12

echo ""
echo "Install audio-libs...

---
This script will
- Take the current user to the group audio
- Add a ppa for mp3gain
- Install:
git jackd
pulseaudio-module-jack qjackctl
audacious
ffmpeg mp3gain lame
Invada PlugIns
Calff Plugins
"
echo "You may this script running at least on ubuntu 20.04!!!"
echo "It will add the ppa:flexiondotorg/audio"
echo "But only, if not exist:"
echo "/etc/apt/sources.list.d/flexiondotorg-ubuntu-audio-focal.list"
echo ""

read -p "Are you sure to proceed? (y/n) " -n 1
echo ""
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
	echo ""
	echo "Installation aborted"
	exit
fi

echo ""
echo "Take user to the audio group"
sudo usermod -aG audio ${USER}

echo ""
echo "PPA for mp3gain..."
echo "Check if it's alraedy there..."

apt_source_file=/etc/apt/sources.list.d/flexiondotorg-ubuntu-audio-focal.list

if [ -f "$apt_source_file" ]; then
	echo "$apt_source_file exists, nothing to do..."
else
	sudo add-apt-repository ppa:flexiondotorg/audio
	sudo sed -i s/focal/bionic/g /etc/apt/sources.list.d/flexiondotorg-ubuntu-audio-focal.list
	sudo apt-get update
fi

echo ""
echo "Load and install packages..."
echo "You may take a cup of coffee..."
echo ""

sudo apt install \
git jackd pulseaudio-module-jack \
audacious ffmpeg qjackctl \
mp3gain lame

echo ""
echo "Invada PlugIns..."
echo ""
sudo apt install invada-studio-plugins-lv2

echo ""
echo "Calf install..."
echo ""
sudo apt install calf-plugins

echo "Finito"
echo "Keep in mind, "
echo "that you have to log out and on "
echo "to have finished adding the user to the audio group"
read -p "Press [Enter] key to finish..."
exit
